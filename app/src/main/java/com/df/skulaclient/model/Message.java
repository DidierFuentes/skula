package com.df.skulaclient.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Didier on 12/6/2015.
 */
public class Message {

	public static final int SHORT_CONTENT_LENGTH = 110;
	private String title;
	private String content;
	private Date date;

//	public static ArrayList<Message> messagesList = new ArrayList<>();
//	static {
//		messagesList.add(new Message("1st Title",
//				"Didier es el mejor",
//				new Date(Calendar.getInstance().get(Calendar.MILLISECOND))));
//		messagesList.add(new Message("2nd Title",
//				"Barbara no es tan bacana",
//				new Date(Calendar.getInstance().get(Calendar.MILLISECOND))));
//		messagesList.add(new Message("3rd Title",
//				"Bubububububu mensaje",
//				new Date(Calendar.getInstance().get(Calendar.MILLISECOND))));
//		messagesList.add(new Message("4th Title",
//				"Bubububububu mensaje 2",
//				new Date(Calendar.getInstance().get(Calendar.MILLISECOND))));
//	}
	public Message(String title, String content, Date date){
		this.title = title;
		this.content = content;
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDateString(){
		// TODO: format date as string
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		String dateString = formatter.format(date);
		return dateString;
	}

	public String toString(){
		return this.content;
	}

	public String getShortContent(){
		if(content.length() > SHORT_CONTENT_LENGTH) {
			return content.substring(0, SHORT_CONTENT_LENGTH) + " ...";
		}
		return content;
	}
}
