package com.df.skulaclient.services;

import android.os.AsyncTask;
import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import java.util.ArrayList;

/**
 * Created by Didier on 12/28/2015.
 */
public class ChannelSubscriberTask extends AsyncTask<ArrayList<String>, Void, Void>{
	/**
	 * Override this method to perform a computation on a background thread. The
	 * specified parameters are the parameters passed to {@link #execute}
	 * by the caller of this task.
	 * <p/>
	 * This method can call {@link #publishProgress} to publish updates
	 * on the UI thread.
	 *
	 * @param params The parameters of the task.
	 * @return A result, defined by the subclass of this task.
	 * @see #onPreExecute()
	 * @see #onPostExecute
	 * @see #publishProgress
	 */
	@SafeVarargs
	@Override
	protected final Void doInBackground(ArrayList<String>... params) {
		ArrayList<String> channels = params[0];
		for(int i = 0; i < channels.size(); i++){
			subscribeToChannel(channels.get(i));
		}

		return null;
	}

	public void subscribeToChannel(final String channel){
		ParsePush.subscribeInBackground(channel, new SaveCallback() {
			@Override
			public void done(ParseException e) {
				if (e == null) {
					Log.d("com.parse.push",
							"successfully subscribed to the " + channel + " channel.");
				} else {
					Log.e("com.parse.push", "failed to subscribe for push", e);
				}
			}
		});
		ParseInstallation.getCurrentInstallation().saveInBackground();
	}
}
