package com.df.skulaclient.services;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Didier on 12/12/2015.
 */
public class GetGcmTokenTask extends AsyncTask<String, Void, String> {

	public interface GcmTokenListener{
		void tokenCallback(String token);
	}

	private ArrayList<GcmTokenListener> listeners;
	private InstanceID instanceID;
	private String projectNumber;

	public GetGcmTokenTask(Context context, String projectNumber){
		instanceID = InstanceID.getInstance(context);
		listeners = new ArrayList<>();
		this.projectNumber = projectNumber;
	}

	@Override
	protected String doInBackground(String... params) {
		String token = "";
		try {
			token = instanceID.getToken(projectNumber,
					GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return token;
	}

	@Override
	protected void onPostExecute(String token){
		for (GcmTokenListener listener : listeners) {
			listener.tokenCallback(token);
		}
	}

	public void addGcmTokenListener(GcmTokenListener listener){
		listeners.add(listener);
	}

}
