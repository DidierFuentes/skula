package com.df.skulaclient.services;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.df.skulaclient.StorageAdapter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Didier on 12/6/2015.
 */
public class WSClient implements GetGcmTokenTask.GcmTokenListener,
						Response.ErrorListener {

	public static final String TAG = "SkulaLog";
	public static final String ERROR_TAG = "SkulaError";
	private final String API_URL = "http://192.168.100.2:8012/skula/api/";
	private final String PROJECT_NUMBER = "18589576982";

	private static WSClient wsClient;
	private Context context;

	private WSClient(Context context){
		this.context = context;
	}

	public static WSClient getInstance(Context context){
		if(wsClient == null){
			wsClient = new WSClient(context);
		}
		return wsClient;
	}

	public void getGcmToken(){
		GetGcmTokenTask task = new GetGcmTokenTask(this.context, PROJECT_NUMBER);
		task.addGcmTokenListener(this);
		task.execute();
	}

	@Override
	public void tokenCallback(String token){
		StorageAdapter.getInstance().storeGcmToken(token, this.context);
		Log.d(TAG, token);
	}

	public void login(final String username, final String password,
					  final Response.Listener<String> listener,
					  final Response.ErrorListener errorListener){

		RequestQueue queue = Volley.newRequestQueue(this.context);
		String url = API_URL + "auth.json";

		StringRequest request = new StringRequest(Request.Method.POST, url,
				listener, errorListener){
			@Override
			protected Map<String, String> getParams(){
				Map<String, String> params = new HashMap<>();
				params.put("username", username);
				params.put("password", password);
				return params;
			}
		};
		queue.add(request);
	}

	public void getMessages(String userId,
							Response.Listener<String> listener,
							Response.ErrorListener errorListener) {

		String url = API_URL + "messages.json?userId="+userId;

		RequestQueue queue = Volley.newRequestQueue(this.context);

		StringRequest request = new StringRequest(Request.Method.GET,
				url, listener, errorListener);

		queue.add(request);
	}

	/**
	 * Callback method that an error has been occurred with the
	 * provided error code and optional user-readable message.
	 *
	 * @param error error returned by server
	 */
	@Override
	public void onErrorResponse(VolleyError error) {
		Log.d(ERROR_TAG, error.toString());
	}
}
