package com.df.skulaclient;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.df.skulaclient.model.Message;

import java.util.List;

/**
 * Created by Didier on 12/6/2015.
 */
public class MessageArrayAdapter extends ArrayAdapter<Message>{

	private Context context;
	private static List<Message> messages;

	/**
	 * Constructor
	 *
	 * @param context  The current context.
	 * @param resource The resource ID for a layout file containing a TextView to use when
	 *                 instantiating views.
	 * @param objects  The objects to represent in the ListView.
	 */
	public MessageArrayAdapter(Context context, int resource, List<Message> objects) {
		super(context, resource, objects);
		this.context = context;
		messages = objects;
	}

	private class ViewHolder {
		TextView title;
		TextView date;
		TextView content;
	}

	public View getView(int position, View view, ViewGroup parent){
		ViewHolder holder;
		Message message = messages.get(position);
		LayoutInflater inflater =
				(LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		if(view == null){
			// Not sure why but doesn't work if passed parent instead of null.
			view = inflater.inflate(R.layout.review_summary, null);

			holder = new ViewHolder();
			holder.title = (TextView) view.findViewById(R.id.review_title);
			holder.date = (TextView) view.findViewById(R.id.review_date);
			holder.content = (TextView) view.findViewById(R.id.review_content);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		holder.title.setText(message.getTitle());
		holder.date.setText(message.getDateString());
		holder.content.setText(message.getShortContent());
		return view;
	}

	public static Message getMessage(int index){
		return messages.get(index);
	}
}
