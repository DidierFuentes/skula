package com.df.skulaclient;

import android.app.Application;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

/**
 * Created by Didier on 12/19/2015.
 */
public class SkulaApplication extends Application {

	private String TAG = "com.parse.push";
	private String appId = "3khkqQzk763K3RwwIo98l9LxNtW7jONktlTtFXmy";
	private String clientKey = "vBCWGxNaVA6puGs8o2DLCL9hcvQ1DoCr8l3nfzm4";
	@Override
	public void onCreate() {
		super.onCreate();

//		WSClient.getInstance(this).getGcmToken();
		Parse.initialize(this, appId, clientKey);

		ParsePush.subscribeInBackground("", new SaveCallback() {
			@Override
			public void done(ParseException e) {
				if (e == null) {
					Log.d("com.parse.push",
							"successfully subscribed to the broadcast channel.");
//					Log.d("com.parse.push", (String) ParseInstallation.getCurrentInstallation().get("deviceToken"));
				} else {
					Log.e("com.parse.push", "failed to subscribe for push", e);
				}
			}
		});
		// If saveInBackground is used before subscribeInBackground
		// doesn't save phone id correctly and push doesnt work
		ParseInstallation.getCurrentInstallation().saveInBackground();
	}
}
