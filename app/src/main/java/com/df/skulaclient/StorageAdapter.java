package com.df.skulaclient;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Didier on 12/13/2015.
 */
public class StorageAdapter {

	private static StorageAdapter adapter;

	private static final String GENERAL_PREFERENCES = "general";

	private static final String GCM_TOKEN = "gcmToken";

	private StorageAdapter(){

	}

	public static StorageAdapter getInstance(){
		if(adapter == null){
			adapter = new StorageAdapter();
		}
		return adapter;
	}

	public void storeGcmToken(String token, Context context){
		SharedPreferences preferences = context.getSharedPreferences(
				GENERAL_PREFERENCES, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(GCM_TOKEN, token);
		editor.apply();
	}
}
