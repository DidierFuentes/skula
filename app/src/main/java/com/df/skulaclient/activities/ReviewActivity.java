package com.df.skulaclient.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.df.skulaclient.MessageArrayAdapter;
import com.df.skulaclient.R;
import com.df.skulaclient.services.WSClient;
import com.df.skulaclient.fragments.MessagesFragment;
import com.df.skulaclient.model.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ReviewActivity extends AppCompatActivity
		implements MessagesFragment.OnFragmentInteractionListener,
		Response.Listener<String>, Response.ErrorListener {

	public static final String USER_ID = "userId";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_review);

		if(getSupportActionBar() != null){
			getSupportActionBar().setTitle(R.string.review_activity_title);
		}

		WSClient.getInstance(this).getMessages(
				String.valueOf(this.getIntent().getIntExtra(USER_ID, 0)), this, this);
		findViewById(R.id.messages_loader).setVisibility(View.VISIBLE);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_review, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_logout) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onFragmentInteraction(String id) {
	}

	/**
	 * Callback method that an error has been occurred with the
	 * provided error code and optional user-readable message.
	 *
	 * @param error error returned by server on request
	 */
	@Override
	public void onErrorResponse(VolleyError error) {
		Log.d(WSClient.TAG, error.toString());
		findViewById(R.id.messages_loader).setVisibility(View.GONE);
	}

	/**
	 * Called when a response is received.
	 *
	 * @param response response returned by server on request
	 */
	@Override
	public void onResponse(String response) {
		((MessagesFragment) getFragmentManager().
			findFragmentById(R.id.messages_list_fragment)).setListAdapter(
				new MessageArrayAdapter(this, R.layout.review_summary , parseMessages(response)));

		findViewById(R.id.messages_loader).setVisibility(View.GONE);
	}

	private List<Message> parseMessages(String messages) {
		List<Message> list = new ArrayList<>();
		try {
			JSONObject json = new JSONObject(messages);
			switch (json.getInt("status")){
				case 200:
					JSONArray messagesJson = json.getJSONArray("messages");
					for (int i = 0; i < messagesJson.length(); i++){
						JSONObject message = messagesJson.getJSONObject(i);

						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
						Date date = format.parse(message.getString("date"));

						list.add(new Message(
								message.getString("title"),
								message.getString("content"),
								date
						));
					}
					break;
				default:
					break;
			}
		} catch (ParseException|JSONException e){
			Log.d(WSClient.TAG, e.toString());
		}

		return list;
	}
}
