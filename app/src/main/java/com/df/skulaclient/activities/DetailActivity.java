package com.df.skulaclient.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.df.skulaclient.MessageArrayAdapter;
import com.df.skulaclient.R;
import com.df.skulaclient.model.Message;

public class DetailActivity extends AppCompatActivity {

	public static final String MESSAGE_IDENTIFIER = "messageID";
	private Message message;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		setTitle("");

		message = MessageArrayAdapter.getMessage(getIntent().getIntExtra(MESSAGE_IDENTIFIER, 0));
		TextView title = (TextView) findViewById(R.id.detail_title);
		title.setText(message.getTitle());
		TextView date = (TextView) findViewById(R.id.detail_date);
		date.setText(message.getDateString());
		TextView content = (TextView) findViewById(R.id.detail_content);
		content.setText(message.getContent());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_logout) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
