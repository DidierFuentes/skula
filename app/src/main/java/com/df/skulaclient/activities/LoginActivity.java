package com.df.skulaclient.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.df.skulaclient.R;
import com.df.skulaclient.services.ChannelSubscriberTask;
import com.df.skulaclient.services.WSClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginActivity extends Activity implements Response.Listener<String>,
		Response.ErrorListener{

	public static final String GENERIC_ERROR = "Couldn't login. Please try again";
	private EditText usernameET;
	private EditText passwordET;
	private Button loginButton;
	private ProgressBar loader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		setUpUI();
//		autoLogin();
	}

	private void autoLogin() {
		usernameET.setText("test");
		passwordET.setText("1234");
		loginButton.callOnClick();
	}

	/*
		Initializes visual components
	 */
	private void setUpUI() {
		usernameET = (EditText) findViewById(R.id.username_edit_text);
		passwordET = (EditText) findViewById(R.id.password_edit_text);
		loginButton = (Button) findViewById(R.id.login_button);
		loader = (ProgressBar) findViewById(R.id.loader);
		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				WSClient.getInstance(v.getContext())
						.login(usernameET.getText().toString(), passwordET.getText().toString(),
								LoginActivity.this, LoginActivity.this);
				loader.setVisibility(View.VISIBLE);
			}
		});
	}

	/**
	 * Called when a response is received.
	 *
	 * @param response response returned by server on request
	 */
	@Override
	public void onResponse(String response) {
		try {
			JSONObject jsonResponse = new JSONObject(response);
			loader.setVisibility(View.GONE);
			switch(jsonResponse.getInt("status")){
				case 200:
					ArrayList<String> channels = new ArrayList<>();
					JSONArray ids = jsonResponse.getJSONArray("studentsID");
					JSONArray grades = jsonResponse.getJSONArray("gradesID");
					for(int i = 0; i < ids.length(); i++){
						channels.add("Student" + ids.getString(i));
					}
					for(int i = 0; i < grades.length(); i++){
						channels.add("Grade" + grades.getString(i));
					}
					//noinspection unchecked
					(new ChannelSubscriberTask()).execute(channels);

					Intent reviewIntent = new Intent(this, ReviewActivity.class);
					reviewIntent.putExtra(ReviewActivity.USER_ID, jsonResponse.getInt("userId"));
					startActivity(reviewIntent);
					break;
				default:
					showDialog(GENERIC_ERROR);
					break;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Callback method that an error has been occurred with the
	 * provided error code and optional user-readable message.
	 *
	 * @param error error returned by server on request
	 */
	@Override
	public void onErrorResponse(VolleyError error) {
		Log.d(WSClient.ERROR_TAG, error.toString());
		loader.setVisibility(View.GONE);
		showDialog(GENERIC_ERROR);
	}

	private void showDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.show();
	}

}
